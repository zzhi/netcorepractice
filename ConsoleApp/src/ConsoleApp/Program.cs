﻿using Serilog;
using System;

namespace ConsoleApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
               .MinimumLevel.Debug()//等级
               .WriteTo.LiterateConsole()//写到控制台
               .WriteTo.RollingFile("logs\\{Date}.txt")//写到文本
               .CreateLogger();

            Log.Information("Hello, world!");
            int a = 10, b = 0;
            try
            {
                Log.Debug("Dividing {A} by {B}", a, b);
                Console.WriteLine(a / b);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Something went wrong");
            }
            Log.CloseAndFlush();
            Console.ReadKey();
        }
    }
}
